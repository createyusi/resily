<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'akihabarajp_stenpureplus' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'akihabarajp_sten' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'v9D4HFPi' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'mysql8009.xserver.jp' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j8M1Kr61 Bw)i[1FU0&1$F<1=.3Odb`~GA21Jc|yj4dy=Z2UoMUe]@jG!q30AT#E' );
define( 'SECURE_AUTH_KEY',  'ZElU#H@/SeI_+tT_:C3 .<]5h*wqLZ/7iKaflnw:,v!R9$uWE4~ytY}12)66$wJY' );
define( 'LOGGED_IN_KEY',    '>n1T^eu!L7r?i{*6gp~V`u[8vxE3A<B32WmEXZIA[])J]e|tmAB;$kk5g5=Wq*_y' );
define( 'NONCE_KEY',        'w@q9gqnq0*lfV%s^UI*03D`Ln!0Jk h?u6X3Le9W#<~}JeR1ZdK>aEs4^)z.v^!e' );
define( 'AUTH_SALT',        'niRr@xADk#KCj;.8Q,A67(i&z7YD4=t(Je+*,[:Slw7y;y#NM~Tw#]wc_b,}=]kT' );
define( 'SECURE_AUTH_SALT', 'vY g_|-rY7zr9^XsRp?aCSC.Q@nVww_n?AaJmQ}z_uV-hzRDa#VleZQ1b,hQo+l|' );
define( 'LOGGED_IN_SALT',   'm4bhC^^b(`BZYn~A>$g&YEu.R8eY0T$XNDooauOzw&i%RK+XD5E1!xak*34^pdd?' );
define( 'NONCE_SALT',       '|QF2Zn~e-,8e(w9UPlXp^hy=;wN%rJ`[,j&cYa3whxON7NE__1@&E6L9 =,75L-T' );
define( 'WP_CACHE_KEY_SALT', 'JiNFvW~XxYkUX6}x>a946${UUuIZKIwC:c1)Bq s25kay~(9a6^k$dP|f/.q)_{S' );
/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define( 'WP_DEBUG', false );

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
