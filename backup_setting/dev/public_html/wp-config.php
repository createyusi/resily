<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'tenpureplus' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'root' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'mysql' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',ov|Q!eEx``z}z@&d#ttMo~a)!y2?W4[}=&(O}9d~TD+!nSVN241B!RhxL1(DA9T' );
define( 'SECURE_AUTH_KEY',  'Ct{} /H^#E5jFCrL?=a4J8#~^)wTLV-w9p3}rdxQGOA%CsmSBR *Ht9cFQ9JHQxc' );
define( 'LOGGED_IN_KEY',    '.j0#Ig36trvmUf(]SAx/35qxb<Xl{Wl0^dzaz%T8p;:nyDg)}==VAq(wCl8w*uc-' );
define( 'NONCE_KEY',        '3Ivg&;Pu<rT&^Zt (,,G*Ef u7v~ |Jqnhzh8x:Ph=%%7<loL1sSBm#X6FWuD<?|' );
define( 'AUTH_SALT',        '`+dpr/`=$sttP_a|6#JW4=nOhpFY2yw+M:X MdxIe~q%l[Q`p+e7RQ.wYK=8.xuJ' );
define( 'SECURE_AUTH_SALT', 'Ce@1QY0lj`Xp[#m=;j/b7s1h01vU>~A-!x#EjS4JbSx]@3# %5SJkK(3|!L8$J+I' );
define( 'LOGGED_IN_SALT',   ':FJ0[Bx1N1DPx?`q/_nA:|wjv+@w6&]P)|MlsO=OLi3Jkp?g>8+ j>9ZzPxjIjBV' );
define( 'NONCE_SALT',       'XmZ3sVt52%1l)v{mn~&>nUeol=@)O3o lPh*~r[n}@?k<b}D >q]:c#|+uZ}1$c0' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define( 'WP_DEBUG', false );

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
