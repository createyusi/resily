# 指図コード
ＤＣＯ３８－ＴＬＭ０１

# ローカル環境
wordpress用のwp-config.phpと
.htaccessはgit管理の対象外としているため、
環境別に調整が必要です。

## docker
### 概要
public_html→出力場所  
workspace→作業場所  
  
workspace配下で
```
docker-compose up -d
```
でdocker起動します

---

### hosts設定
C:\Windows\System32\drivers\etc\hosts  
```
127.0.0.1 tenpureplus.local
```
を追加  

---

### SSL設定（設定済み、自己証明書は再設定が必要）
workspace/nginx/certsにkeyとpemを作成して格納  
workspace/nginx/default.confに作成したkeyとpemのパスを記載
```
server {
  listen 443 ssl http2;
  ...

  ssl on;
  ssl_certificate /etc/certs/tenpureplus.local.pem;
  ssl_certificate_key /etc/certs/tenpureplus.local-key.pem;
  ssl_protocols SSLv3 TLSv1.2;

  ...
```
workspace/nginx/Dcokerfileにdocker内部へコピーするkeyとpemのパスを記載
```
ADD ./certs/tenpureplus.local.pem /etc/certs/tenpureplus.local.pem
ADD ./certs/tenpureplus.local-key.pem /etc/certs/tenpureplus.local-key.pem
```
---

### BASIC認証設定（設定済み）
workspace/nginx/default.confに下記記載
```
# Basic認証用の設定を追加
auth_basic "Restricted";  # 表示される文字
auth_basic_user_file /etc/nginx/.htpasswd;  # 作成したBasic認証用のファイル
```
workspace/nginx/htpasswd/.htpasswdにIDとPASSを記載（passwordは文字変換後のもの）
```
jtuser2020:rvkTxAVzMogkw
```
workspace/nginx/Dcokerfileにdocker内部へコピーするpasswordのパスを記載
```
ADD ./htpasswd/.htpasswd /etc/nginx/.htpasswd
```
---

### vue
workspace/vueでwordpress用のテーマを作成しています。  
workspace/vueのディレクトリで、
```
npm run build
```
でテーマを生成します。

---

## DB
http://localhost:8000/  
ID:root  
PASS:root  
MySQLデータベース名:tenpureplus  
MySQLユーザー名:root  
MySQLパスワード:root  
メールアドレス s-oshima@jt-s.com  

https://phpmyadmin-sv8084.xserver.jp/

【ステージング】
MySQLユーザー名:akihabarajp_sten
MySQLパスワード:v9D4HFPi
ホスト:mysql8009.xserver.jp

【本番】
MySQLユーザー名:akihabarajp_ten
MySQLパスワード:M7pvXCY8
ホスト:mysql8009.xserver.jp
---

## ログイン
※hostsの設定、docker起動後にアクセス可能  
<!-- https://tenpureplus.local/  
https://tenpureplus.local/twm-ms   -->
BASIC ID:mytest  
BASIC PASS:mytest  
ユーザー名:tenpureplus_rooter  
パスワード:$6Ne5VjYCuFO 

本番パスワード:$6Ne5VjYCuFO  

# URL（詳細はwebサイト構成定義書記載）
## 本番
https://tenpureplus.com
## テスト
https://tenpureplus.jt-akihabarajp.work/




---------------------
お問い合わせ関連（設定済み）
---------------------
お問い合わせフォーム4種類分、
自動返信と通知メール設定済みです。
自動返信：フォーム入力欄のメールアドレスに返信
通知メール：仮で斉藤さんのメールアドレスに連絡

仮の返信文言を入れているので、
来週打ち合わせで修正したいと思います。

・お問い合わせ
https://tenpureplus.jt-akihabarajp.work/inquiry
・お申込み
https://tenpureplus.jt-akihabarajp.work/templates/movie2
・テンプレート改変（お申込みフォーム内の改変相談ボタンより遷移）
https://tenpureplus.jt-akihabarajp.work/templates/movie2
・オリジナル動画相談
https://tenpureplus.jt-akihabarajp.work/inquiryOrdering

---------------------
サイトの使い方
---------------------
使い方動画を入れる想定で枠組みがあります。
仮でミツバのままになっています。
行程の画像キャプチャがまだアタリ画像となっています。
手順が上がってきているので、来週それにあった画像を作成します。

---------------------
クマ
---------------------
クマ画像同じものをそこかしこに入れていますが、
シチュエーションでイラストをわけたいです。
例）お問い合わせボタン→メールのイラスト

---------------------
マイクロアニメーション
---------------------
ボタンのマウスオン時のアニメーションが無い箇所を
来週作ります。

---------------------
個人情報保護方針、会社概要、利用規約
---------------------
内容確定後に差し替えます。

---------------------
iphone
---------------------
ナビゲーションがうまく表示されない問題を、
来週macproに接続してデバッグします。

---------------------
コロナ関連
---------------------
3コマ漫画部分のインバウンド関連見直し

---------------------
本番ドメイン
---------------------
来週取得後に、xサーバーに本番領域を作成します。

---------------------
管理画面操作＆公開前チェックリスト
---------------------
来週資料を用意します。
特にお問い合わせ回りのデバッグは大藪さんと相談してリスト作ります。
また、追加で西村さんが作成してくれている動画も、
管理画面を通してテンプレート追加を来週行います。（その際に手順書を作成する）