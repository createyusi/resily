<?php
/**
 * terasuline wide medical Theme functions
 *
 * @package WordPress
 * @subpackage terasuline wide medical
 */

 /**
 * REST APIのurlを変更（wordpress設定→パーマリンク設定→更新）
 * https://qiita.com/kuck1u/items/c879271aa280da62c573
 */
add_filter( 'rest_url_prefix', function () {
  return 'api';
} );

//絶対パス→相対パス
function make_href_root_relative($input) {
  return preg_replace('!http(s)?://' . $_SERVER['SERVER_NAME'] . '/!', '/', $input);
}

/**
 * wordpress標準設定解除
 */
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');
add_filter( 'emoji_svg_url', '__return_false' );
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
function deregister_item() {
  wp_deregister_script('jquery'); // jquery
  //pluginのcssを削除
  // wp_dequeue_style( 'contact-form-7' );
  wp_dequeue_style( 'wp-block-library' );
  wp_dequeue_style( 'wp-rest-filter' );
}
add_action( 'wp_enqueue_scripts', 'deregister_item' );

/**
 * 成者アーカイブの無効化 ?author=nアクセスを禁止
 */
function disable_author_archive_query() {
  if( preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING']) ){
    wp_redirect( home_url() );
    exit;
  }
}
add_action('init', 'disable_author_archive_query');
/**
 * コメント著者クラスの削除
 */
function remove_comment_author_class( $classes ) {
	foreach ( $classes as $key => $class ) {
		if ( strstr( $class, 'comment-author-' ) ) {
			unset( $classes[$key] );
		}
	}
	return $classes;
}
add_filter( 'comment_class', 'remove_comment_author_class', 10, 1 );

/**
 * テーマ・プラグイン編集不可
 */
define('DISALLOW_FILE_EDIT', true);
// define('DISALLOW_FILE_MODS', true);


/**
 * カスタム投稿（template）追加
 * https://wp-principle.net/taxonomy/
 * https://www.imamura.biz/blog/31465
 */
function create_post_type() {
    register_post_type( 'templates', [ // 投稿タイプ名の定義
        'labels' => [
            'name'          => __( '動画テンプレート' ), // 管理画面上で表示する投稿タイプ名
            'add_new_item' => __( '動画テンプレートを追加' ),
            'edit_item' => __( '動画テンプレートの編集' ),
            'singular_name' => 'templates',    // カスタム投稿の識別名
        ],
        'public'        => true,  // 投稿タイプをpublicにするか
        'has_archive'   => false, // アーカイブ機能ON/OFF
        'menu_position' => 5,     // 管理画面上での配置場所
        'show_in_rest'  => true,  // 5系から出てきた新エディタ「Gutenberg」を有効にする
        'rest_base' => 'templates',
        'hierarchical' => false, 
    ]);
    register_taxonomy( //　カスタムタクソノミー（カテゴリタイプ）
      'uses',// 用途 カスタムタクソノミー名
      'templates',// カスタムタクソノミーを反映させる投稿タイプの定義名
      array(
        'label' => __( '用途' ),// 表示するカスタムタクソノミー名
        'show_in_rest' => true,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
      )
    );
    register_taxonomy( //　カスタムタクソノミー（カテゴリタイプ）
      'seasons',// 季節 カスタムタクソノミー名
      'templates',// カスタムタクソノミーを反映させる投稿タイプの定義名
      array(
        'label' => __( '季節・時期' ),// 表示するカスタムタクソノミー名
        'show_in_rest' => true,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
      )
    );
    register_taxonomy(
      'templatesTags', //タグ名（任意）
      'templates', //カスタム投稿名
      array(
        'update_count_callback' => '_update_post_term_count',
        //ダッシュボードに表示させる名前
        'label' => 'タグ', 
        'show_in_rest' => true,
        'public' => true,
        'show_ui' => true,
        'hierarchical' => false, //タグタイプの指定（階層をもたない）
      )
    );
}

/**
 * templates一覧へ項目を追加
 * https://b-risk.jp/blog/2017/02/wp_admin_list_columns/
 */
function my_manage_posts_columns_uses( $defaults ) {
  $new_columns = array();
  foreach ( $defaults as $column_name => $column_display_name ) {
      if ( $column_name == 'date' ) {
          $new_columns['uses'] = '用途';
          $new_columns['seasons'] = '季節・時期';
          $new_columns['templatesTags'] = 'タグ';
      }
      $new_columns[ $column_name ] = $column_display_name;
  }   
  return $new_columns;
}


/**
 * templates一覧へ追加した項目の値を記載　カスタムフィールドの値を表示
 */
function my_add_column($column_name, $post_id) {
  $new_ar = array('uses','seasons','templatesTags');
  foreach ( $new_ar as $item ) {
    if( $column_name == $item ) {
      $tax = wp_get_object_terms($post_id, $item);
      foreach ($tax as $itemChild) {
        echo esc_attr($itemChild->name) . "<br>";
      }
    }
  }
}

/**
 * templates一覧へ標準以外の絞込機能を追加
 */
function my_add_post_taxonomy_restrict_filter() {
  global $post_type;
  if ( 'templates' == $post_type ) {
?>
    <select name="uses">
      <option value="">用途指定なし</option>
<?php
      $terms = get_terms('uses');
      foreach ($terms as $term) { 
?>
        <option value="<?php echo $term->slug; ?>" <?php if ( $_GET['uses'] == $term->slug ) { print 'selected'; } ?>><?php echo $term->name; ?></option>
<?php
      }
?>
    </select>
    <select name="seasons">
      <option value="">季節・時期指定なし</option>
<?php
      $terms = get_terms('seasons');
      foreach ($terms as $term) {
?>
        <option value="<?php echo $term->slug; ?>" <?php if ( $_GET['seasons'] == $term->slug ) { print 'selected'; } ?>><?php echo $term->name; ?></option>
<?php
      }
?>
    </select>
    <select name="templatesTags">
      <option value="">タグ指定なし</option>
<?php
      $terms = get_terms('templatesTags');
      foreach ($terms as $term) {
?>
        <option value="<?php echo $term->slug; ?>" <?php if ( $_GET['templatesTags'] == $term->slug ) { print 'selected'; } ?>><?php echo $term->name; ?></option>
<?php
      }
?>
    </select>
<?php
  }
}
function adjust_width_modified_column() {
  $data = <<<EOT
  .fixed .column-uses,
  .fixed .column-seasons { width: 12%; }
  .fixed .column-templatesTags { width: 15%; }
  @media screen and (max-width: 1320px) {
    .fixed .column-date,
    .fixed .column-uses,
    .fixed .column-seasons,
    .fixed .column-templatesTags { width: 12.5%; }
  }
EOT;
	wp_add_inline_style( 'list-tables', $data );
}
add_action( 'init', 'create_post_type' );
add_filter( 'manage_edit-templates_columns', 'my_manage_posts_columns_uses' );
add_action( 'manage_templates_posts_custom_column', 'my_add_column', 10, 2 );
add_action( 'restrict_manage_posts', 'my_add_post_taxonomy_restrict_filter' );
add_action( 'admin_print_styles', 'adjust_width_modified_column' );


