import axios from "axios";
const API_URI = process.env.VUE_APP_SITEURL + "api/wp/v2/seasons";
export const SEASONSAPI = {
  getSeasons(callback, queryParams) {
    let query = queryParams;
    if (typeof window.WP_API_Settings === "object") {
      query = query.length
        ? query + "&status=publish,private,draft&_wpnonce=" + window.WP_API_Settings.nonce
        : "?status=publish,private,draft&_wpnonce=" + window.WP_API_Settings.nonce;
    }
    axios
      .get(API_URI + query)
      .then(response => {
        callback(response.data);
      })
      .catch(error => {
        callback(error);
        // console.log(error);
      });
  }
};
