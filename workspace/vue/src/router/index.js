import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import(/* webpackChunkName: "home" */ "../views/pageHome.vue")
    },
    // 翻訳参考例
    {
      path: "/translation",
      name: "translation",
      component: () => import(/* webpackChunkName: "translation" */ "../views/pageTranslation.vue")
    },
    // サイトの使い方
    {
      path: "/howto",
      name: "howto",
      component: () => import(/* webpackChunkName: "howto" */ "../views/pageHowto.vue")
    },
    // 用途で選ぶ
    {
      path: "/uses",
      name: "uses",
      component: () => import(/* webpackChunkName: "uses" */ "../views/pageUses.vue")
    },
    // 季節・時期で選ぶ
    {
      path: "/seasons",
      name: "seasons",
      component: () => import(/* webpackChunkName: "season" */ "../views/pageSeasons.vue")
    },
    // テンプレート紹介
    {
      path: "/templates/:slug",
      name: "templates",
      component: () => import(/* webpackChunkName: "templates" */ "../views/pageTemplates.vue")
    },
    // 記事一覧
    {
      path: "/articles",
      name: "articles",
      component: () => import(/* webpackChunkName: "articles" */ "../views/pageArticles.vue")
    },
    // 記事詳細&お申込み
    {
      path: "/articleDetail",
      name: "articleDetail",
      component: () => import(/* webpackChunkName: "articleDetail" */ "../views/pageArticleDetail.vue")
    },
    // お申込み完了
    {
      path: "/thanksTemplate",
      name: "thanksTemplate",
      component: () => import(/* webpackChunkName: "thanksTemplate" */ "../views/pageThanksTemplate.vue")
    },
    // お問合せ[直接発注]
    {
      path: "/inquiryOrdering",
      name: "inquiryOrdering",
      component: () => import(/* webpackChunkName: "inquiryOrdering" */ "../views/pageInquiryOrdering.vue")
    },
    // お問合せ[直接発注]完了
    {
      path: "/thanksOrdering",
      name: "thanksOrdering",
      component: () => import(/* webpackChunkName: "thanksOrdering" */ "../views/pageThanksOrdering.vue")
    },
    // お問合せ[テンプレート改変]
    // {
    //   path: "/inquiryAlteration",
    //   name: "inquiryAlteration",
    //   component: () => import(/* webpackChunkName: "pageInquiryAlteration" */ "../views/pageInquiryAlteration.vue")
    // },
    // お問合せ[テンプレート改変]完了
    // {
    //   path: "/thanksAlteration",
    //   name: "thanksAlteration",
    //   component: () => import(/* webpackChunkName: "thanksAlteration" */ "../views/pageThanksAlteration.vue")
    // },
    // お問合せ[総合]
    {
      path: "/inquiry",
      name: "inquiry",
      component: () => import(/* webpackChunkName: "inquiry" */ "../views/pageInquiry.vue")
    },
    // お問合せ[総合]完了
    {
      path: "/thanksInquiry",
      name: "thanksInquiry",
      component: () => import(/* webpackChunkName: "thanksInquiry" */ "../views/pageThanksInquiry.vue")
    },
    // 個人情報保護方針
    {
      path: "/policy",
      name: "policy",
      component: () => import(/* webpackChunkName: "policy" */ "../views/pagePolicy.vue")
    },
    // 利用規約
    {
      path: "/terms",
      name: "terms",
      component: () => import(/* webpackChunkName: "terms" */ "../views/pageTerms.vue")
    },
    // 会社概要
    // {
    //   path: "/company",
    //   name: "company",
    //   component: () => import(/* webpackChunkName: "company" */ "../views/pageCompany.vue")
    // },
    {
      path: "/:name*", // 該当するページが存在しない場合は404ページに遷移する
      name: "notfound",
      component: () => import(/* webpackChunkName: "company" */ "../views/pageNotFound.vue")
    }
  ]
  //  scrollBehavior(to, from, savedPosition) {
  //    return { x: 0, y: 0 };
  //  }
});
