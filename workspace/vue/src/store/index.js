import Vue from "vue";
import Vuex from "vuex";
import { TEMPLATESAPI } from "../api/templates";
import { USESAPI } from "../api/uses";
import { SEASONSAPI } from "../api/seasons";
import store from "@/store";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    templates: [], // 動画テンプレート情報
    templatesGetNum: 100, // 動画テンプレート一回に取得する件数
    templatesLoaded: false, // 動画テンプレート取得済みフラグ
    uses: [], // 用途情報
    usesLoaded: false, // 用途取得済みフラグ
    seasons: [], // 季節情報
    seasonsLoaded: false, // 季節取得済みフラグ
    templatesUses: [], // 用途別の動画テンプレート情報
    templatesUsesLoaded: false, // 用途別の動画テンプレート取得済みフラグ
    templatesSeasons: [], // 季節別の動画テンプレート情報
    templatesSeasonsLoaded: false // 季節別の動画テンプレート取得済みフラグ
  },
  getters: {
    templatesUses: state => {
      // 配列をobjectにまとめて返す
      // 例）{spring:"...",summer:"....",...}
      let ar = {};
      state.templatesUses.forEach(element => {
        Object.assign(ar, element);
      });
      return ar;
    },
    templatesSeasons: state => {
      // 配列をobjectにまとめて返す
      // 例）{spring:"...",summer:"....",...}
      let ar = {};
      state.templatesSeasons.forEach(element => {
        Object.assign(ar, element);
      });
      // console.log(ar);
      return ar;
    },
    templatesPickup: state => {
      // 配列をobjectにまとめて返す
      // 例）{spring:"...",summer:"....",...}
      let ar = [];
      // console.log(state.templates);
      state.templates.forEach(element => {
        if (element.acf.pickupshow[0]) {
          ar.push(element);
        }
      });
      return ar;
    }
  },
  mutations: {
    set_templates(state, templates) {
      // 取得エラーの場合はエラーをstateに格納
      if (String(templates).match(/^Error/)) {
        // エラーを格納
        state.templates = "error";
      } else {
        // apiの取得値（動画テンプレート情報）をstateに格納
        state.templates = templates;
      }
      // 完了フラグをtrue
      state.templatesLoaded = true;
    },
    // 用途格納
    set_uses(state, uses) {
      state.uses = uses;
      state.usesLoaded = true;
      // 用途ごとのテンプレート取得
      Object.keys(state.uses).forEach(index => {
        let usesSlug = state.uses[index].slug;
        let param = {
          query: "?filter[uses]=" + usesSlug,
          type: "uses",
          slug: usesSlug
        };
        store.dispatch("get_templates", param);
      });
    },
    // 季節・時期格納
    set_seasons(state, seasons) {
      state.seasons = seasons;
      state.seasonsLoaded = true;
      // 季節・時期ごとのテンプレート取得
      Object.keys(state.seasons).forEach(index => {
        let seasonsSlug = state.seasons[index].slug;
        let param = {
          query: "?filter[seasons]=" + seasonsSlug,
          type: "seasons",
          slug: seasonsSlug
        };
        store.dispatch("get_templates", param);
      });
    },
    // 用途用テンプレート情報格納
    set_templatesUses(state, data) {
      // 用途名オブジェクトで配列に格納※直オブジェクトだと監視がうまく動かない
      state.templatesUses.push({ [data.slug]: data.value });
    },
    // 季節・時期用テンプレート情報格納
    set_templatesSeasons(state, data) {
      // 季節・時期名オブジェクトで配列に格納※直オブジェクトだと監視がうまく動かない
      state.templatesSeasons.push({ [data.slug]: data.value });
    }
  },
  actions: {
    // テンプレート情報取得
    get_templates({ commit }, param) {
      if (param.type === "uses") {
        TEMPLATESAPI.getTemplates(templates => {
          let data = {
            slug: param.slug,
            value: templates
          };
          commit("set_templatesUses", data);
        }, param.query);
      } else if (param.type === "seasons") {
        TEMPLATESAPI.getTemplates(templates => {
          let data = {
            slug: param.slug,
            value: templates
          };
          commit("set_templatesSeasons", data);
        }, param.query);
      } else {
        TEMPLATESAPI.getTemplates(templates => {
          commit("set_templates", templates);
        }, param.query);
      }
    },
    // 用途一覧を取得
    get_uses({ commit }, queryParams) {
      USESAPI.getUses(uses => {
        commit("set_uses", uses);
      }, queryParams);
    },
    // 季節・時期一覧を取得
    get_seasons({ commit }, queryParams) {
      SEASONSAPI.getSeasons(seasons => {
        commit("set_seasons", seasons);
      }, queryParams);
    }
  }
});

// 共通データ取得
let templatesParams = {
  type: "new",
  query: "?per_page=" + store.state.templatesGetNum
};
store.dispatch("get_templates", templatesParams);

let usesQueryParams = "/";
store.dispatch("get_uses", usesQueryParams);

let seasonsQueryParams = "/";
store.dispatch("get_seasons", seasonsQueryParams);
