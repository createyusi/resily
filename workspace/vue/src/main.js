import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VCalendar from "v-calendar";
import VueScrollTo from "vue-scrollto";
import VueObserveVisibility from "vue-observe-visibility";
import VueYoutube from "vue-youtube";
// import VueAnalytics from "vue-analytics";

if (
  navigator.userAgent.match(/MSIE 10/i) ||
  navigator.userAgent.match(/Trident\/7\./) ||
  navigator.userAgent.match(/Edge\/12\./) ||
  navigator.userAgent.match(/iP(hone|(o|a)d)/)
) {
  require("intersection-observer");
}

// カレンダー
Vue.use(VCalendar, {
  componentPrefix: "vc"
});

/**
 * youtube埋め込み用
 * https://www.npmjs.com/package/vue-youtube
 * https://qiita.com/howdy39/items/bede05cb1ff9f7ab2783
 */
Vue.use(VueYoutube);

// memo
// https://qiita.com/g-taguchi/items/9f97f2172aa048934f1c

// これなんだっけ？
Vue.config.productionTip = false;

/**
 * スクロール値検出用
 * https://github.com/Akryum/vue-observe-visibility
 */
Vue.use(VueObserveVisibility);

/**
 * スクロールアニメーション
 * https://github.com/rigor789/vue-scrollto
 */
Vue.use(VueScrollTo);

/**
 * analytics設定
 * https://github.com/MatteoGabriele/vue-analytics/blob/master/docs/opt-out.md
 * IEで「オブジェクトは 'find' プロパティまたはメソッドをサポートしていません。」と表示される
 */
// Vue.use(VueAnalytics, {
//   id: "UA-158110588-1",
//   router
// });

/**
 * Vueスタート
 */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#tenpureplus");
