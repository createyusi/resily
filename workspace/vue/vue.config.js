/**
 * vue全体設定
 */

/**
 * ライブラリ読み込み
 */
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ImageminPlugin = require("imagemin-webpack-plugin").default;
const imageminMozjpeg = require("imagemin-mozjpeg");

/**
 * 開発モードをdevModeで定義
 */
const devMode = process.env.NODE_ENV !== "production";
module.exports = {
  transpileDependencies: ["v-calendar"],
  publicPath: "/", // router関連など共通のパス
  outputDir: "../../public_html/wp-content/themes/tenpureplus", // 書き出し場所
  productionSourceMap: false, // ソースマップは無し
  pages: {
    top: devMode
      ? {
          entry: "src/main.js",
          template: "public/index.html", // テンプレートのhtml
          filename: "index.html" // build時に出力されるファイル名
        }
      : {
          entry: "src/main.js",
          template: "public/index.php", // テンプレートのphp
          filename: "index.php" // build時に出力されるファイル名
        }
  },
  css: devMode
    ? {}
    : {
        extract: {
          filename: "../../../assets/css/[name].css",
          chunkFilename: "../../../assets/css/[id].css"
        }
      },
  chainWebpack: config => {
    config.output.filename(devMode ? "js/[name].[hash].js" : "../../../assets/js/[name].js");
    config.output.chunkFilename(devMode ? "js/[name].[hash].js" : "../../../assets/js/[name].js");
    config.module
      .rule("images")
      .test(/\.(png|jpe?g|gif|ico|webp)(\?.*)?$/)
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: 2048,
        name: devMode ? "images/[name].[hash].[ext]" : "../../../assets/images/[name].[ext]"
      });
    config.module
      .rule("svg")
      .test(/\.(svg)(\?.*)?$/)
      .use("file-loader")
      .loader("file-loader")
      .options({
        name: devMode ? "images/[name].[hash].[ext]" : "../../../assets/images/[name].[ext]"
      });
    config.module
      .rule("media")
      .test(/\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/)
      .use("url-loader")
      .loader("url-loader")
      .options({
        limit: 4096,
        name: devMode ? "media/[name].[hash].[ext]" : "../../../assets/media/[name].[ext]"
      });
  },
  configureWebpack: {
    plugins: [
      /**
       * pathが繋がらない画像を出力ディレクトリへ複製
       * https://github.com/webpack-contrib/copy-webpack-plugin
       */
      new CopyWebpackPlugin([
        {
          from: "src/images/favicon.ico",
          to: "../../../assets/images/favicon.ico"
        },
        {
          from: "src/images/ogp.jpg",
          to: "../../../assets/images/ogp.jpg"
        }
      ]),
      /**
       * 画像を圧縮して書き出し
       */
      new ImageminPlugin({
        disable: devMode, // 開発モードでは実行しない
        pngquant: {
          quality: "75-100"
        },
        plugins: [
          imageminMozjpeg({
            quality: 75
          })
        ]
      })
    ]
  },
  pluginOptions: {
    proxy: {
      enabled: true,
      context: "/api",
      options: {
        target: "https://localhost",
        changeOrigin: true
      }
    }
  }
};
